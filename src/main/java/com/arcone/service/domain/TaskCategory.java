package com.arcone.service.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TaskCategory {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String categoryName;
	private Boolean active;

	public TaskCategory() {
	}

	public TaskCategory(String categoryName, Boolean active) {
		super();
		this.categoryName = categoryName;
		this.active = active;
	}

	public TaskCategory(Long id, String categoryName, Boolean active) {
		super();
		this.id = id;
		this.categoryName = categoryName;
		this.active = active;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "TaskCategory [id=" + id + ", categoryName=" + categoryName + ", active=" + active + "]";
	}

}
