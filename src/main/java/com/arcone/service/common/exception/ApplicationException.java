package com.arcone.service.common.exception;

/**
 * Represents the controlled application exception.
 */
public class ApplicationException extends Exception {
    private static final long serialVersionUID = 5959526869767546754L;
    /**
     * Represents the public-facing error message.
     */
    private String detail;
    /**
     * Represents the not public-facing error message. Contain the exception details that developers will see.
     */
    private String developerMessage;
    /**
     * @param detailError the public-facing error message.
     * @param developerMessageError the exception details for developers.
     */
    public ApplicationException(String detailError, String developerMessageError) {
        super(detailError);
        this.detail = detailError;
        this.developerMessage = developerMessageError;
    }
    /**
     * @return the detail of the public-facing error message
     */
    public String getDetail() {
        return detail;
    }
    /**
     * Functions that set the value for detail (public-facing error message).
     * @param detailError the detail error message to set
     */
    public void setDetail(String detailError) {
        this.detail = detailError;
    }
    /**
     * @return the developerMessage value of developer error message
     */
    public String getDeveloperMessage() {
        return developerMessage;
    }
    /**
     * Functions that set the value for developerMessage.
     * @param developerMessageError the developerMessage to set
     */
    public void setDeveloperMessage(String developerMessageError) {
        this.developerMessage = developerMessageError;
    }
}
