package com.arcone.service.common.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class JsonDateSerializer extends JsonSerializer<Date> {

	@Override
	public void serialize(Date value, JsonGenerator gen, SerializerProvider provider) throws IOException, JsonProcessingException {
		try {
		    final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			String formattedDate = dateFormat.format(value);
			gen.writeString(formattedDate);
		} catch(IOException ioe) {
			throw ioe;
		} 
	}
}
