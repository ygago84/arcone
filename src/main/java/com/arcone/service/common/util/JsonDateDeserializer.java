package com.arcone.service.common.util;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class JsonDateDeserializer extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonParser jsonparser, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        String date = jsonparser.getText();
        try {
            Date resultDate = dateFormat.parse(date);
            return resultDate;
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

}
