package com.arcone.service.published.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arcone.service.common.exception.ApplicationException;
import com.arcone.service.features.task.assembler.TaskAssembler;
import com.arcone.service.features.task.dto.TaskDto;
import com.arcone.service.features.task.service.TaskService;
import com.arcone.service.features.taskcategory.assembler.TaskCategoryAssembler;
import com.arcone.service.features.taskcategory.dto.TaskCategoryDto;
import com.arcone.service.features.taskcategory.service.TaskCategoryService;
import com.arcone.service.published.TaskPublishedService;

@Service
@Transactional
public class TaskPublishedServiceImpl implements TaskPublishedService {
	private static final Logger LOGGER = LoggerFactory.getLogger(TaskPublishedServiceImpl.class);

	@Autowired
	TaskService taskService;
	@Autowired
	TaskCategoryService taskCategoryService;

	@Override
	public List<TaskCategoryDto> getAllCategories() throws ApplicationException {
		try {
			return TaskCategoryAssembler.toDtos(taskCategoryService.getAllCategories());
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw new ApplicationException("An error occurred trying to get all task category.", e.getMessage());
		}
	}

	@Override
	public TaskCategoryDto save(TaskCategoryDto categoryDto) throws ApplicationException {
		try {
			return TaskCategoryAssembler.toDto(Optional.of(taskCategoryService.save(categoryDto)));
		} catch (ApplicationException e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw new ApplicationException("An error occurred trying to save a category.", e.getMessage());
		}
	}

	@Override
	public void deleteTaskCategory(Long categoryId) throws ApplicationException {
		try {
			taskCategoryService.delete(categoryId);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw new ApplicationException("An error occurred trying to delete a category.", e.getMessage());
		}
	}

	@Override
	public TaskDto save(TaskDto taskDto) throws ApplicationException {
		try {
			return TaskAssembler.toDto(taskService.save(taskDto));
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw new ApplicationException("An error occurred trying to save a task.", e.getMessage());
		}
	}

	@Override
	public void deleteTask(Long taskId) throws ApplicationException {
		try {
			taskService.delete(taskId);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw new ApplicationException("An error occurred trying to delete a task.", e.getMessage());
		}
	}
	
	@Override
	public List<TaskDto> getAllTasks() throws ApplicationException {
		try {
			return TaskAssembler.toDtos(taskService.getAllTasks());
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw new ApplicationException("An error occurred trying to get all task category.", e.getMessage());
		}
	}

}
