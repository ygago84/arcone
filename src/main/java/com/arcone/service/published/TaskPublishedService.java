package com.arcone.service.published;

import java.util.List;

import com.arcone.service.common.exception.ApplicationException;
import com.arcone.service.features.task.dto.TaskDto;
import com.arcone.service.features.taskcategory.dto.TaskCategoryDto;

public interface TaskPublishedService {
	public List<TaskCategoryDto> getAllCategories() throws ApplicationException;
	public TaskCategoryDto save(TaskCategoryDto categoryDto) throws ApplicationException;
    public void deleteTaskCategory(Long categoryId) throws ApplicationException;
    
    public List<TaskDto> getAllTasks() throws ApplicationException;
    public TaskDto save(TaskDto taskDto) throws ApplicationException;
    public void deleteTask(Long taskId) throws ApplicationException;
}
