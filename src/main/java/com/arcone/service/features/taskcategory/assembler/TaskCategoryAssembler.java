package com.arcone.service.features.taskcategory.assembler;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.arcone.service.domain.TaskCategory;
import com.arcone.service.features.taskcategory.dto.TaskCategoryDto;

public class TaskCategoryAssembler {

	public static TaskCategoryDto toDto(Optional<TaskCategory> taskCategoryOptional) {
		TaskCategoryDto categoryDto = new TaskCategoryDto();
		taskCategoryOptional.ifPresent(taskCateg -> {
			categoryDto.setId(taskCategoryOptional.get().getId());
			categoryDto.setCategoryName(taskCategoryOptional.get().getCategoryName());
			categoryDto.setActive(taskCategoryOptional.get().getActive());
		});
		return categoryDto;
	}

	public static List<TaskCategoryDto> toDtos(Optional<Iterable<TaskCategory>> categoriesOptional) {
		List<TaskCategoryDto> dtos = new ArrayList<>();
		categoriesOptional.ifPresent(categoryCollection -> {
			categoryCollection
					.forEach(taskCategory -> dtos.add(TaskCategoryAssembler.toDto(Optional.ofNullable(taskCategory))));
		});
		return dtos;
	}

	public static TaskCategory toEntity(TaskCategoryDto categoryDto) {
		return null != categoryDto.getId() ? new TaskCategory(categoryDto.getId(), categoryDto.getCategoryName(), categoryDto.getActive())
				: new TaskCategory(categoryDto.getCategoryName(), true);
	}

}
