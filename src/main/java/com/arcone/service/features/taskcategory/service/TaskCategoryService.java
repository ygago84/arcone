package com.arcone.service.features.taskcategory.service;

import java.util.Optional;

import com.arcone.service.domain.TaskCategory;
import com.arcone.service.features.taskcategory.dto.TaskCategoryDto;

public interface TaskCategoryService {
	public TaskCategory save(TaskCategoryDto categoryDto) throws Exception;
	public void delete(Long categoryId) throws Exception;
    public Optional<Iterable<TaskCategory>> getAllCategories() throws Exception;
}
