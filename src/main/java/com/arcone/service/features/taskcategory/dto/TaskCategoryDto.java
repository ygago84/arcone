package com.arcone.service.features.taskcategory.dto;

import java.io.Serializable;

public class TaskCategoryDto implements Serializable {
	private static final long serialVersionUID = 1367581180680229799L;
	private Long id;
	private String categoryName;
	private Boolean active;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "TaskCategoryDto [id=" + id + ", categoryName=" + categoryName + ", active=" + active + "]";
	}

}
