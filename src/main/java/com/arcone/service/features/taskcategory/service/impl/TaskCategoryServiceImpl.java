package com.arcone.service.features.taskcategory.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arcone.service.common.exception.ApplicationException;
import com.arcone.service.domain.TaskCategory;
import com.arcone.service.features.taskcategory.assembler.TaskCategoryAssembler;
import com.arcone.service.features.taskcategory.dto.TaskCategoryDto;
import com.arcone.service.features.taskcategory.repository.TaskCategoryRepository;
import com.arcone.service.features.taskcategory.service.TaskCategoryService;

@Service
public class TaskCategoryServiceImpl implements TaskCategoryService {
    @Autowired
    TaskCategoryRepository taskCategoryRepository; 
    
    @Override
    public Optional<Iterable<TaskCategory>> getAllCategories() throws Exception {
        return Optional.of(taskCategoryRepository.findByActive(true));
    }

    @Override
    public TaskCategory save(TaskCategoryDto categoryDto) throws Exception {
    	TaskCategory taskCategory = taskCategoryRepository.findByCategoryNameIgnoreCase(categoryDto.getCategoryName());
    	if (taskCategory == null) {
    		return taskCategoryRepository.save(TaskCategoryAssembler.toEntity(categoryDto));
    	} else if (taskCategory.getActive() == false) {
    		taskCategory.setActive(true);
    		return taskCategory;
    	} else {
    		throw new ApplicationException("Category already exist", "Category already exist");
    	}
    }

    @Override
    public void delete(Long categoryId) throws Exception {
    	Optional<TaskCategory> taskToDelete = taskCategoryRepository.findById(categoryId);
    	taskToDelete.ifPresent(task->{
    		task.setActive(false);
    		taskCategoryRepository.save(task);
    	});
    }

}
