package com.arcone.service.features.taskcategory.repository;

import org.springframework.data.repository.CrudRepository;

import com.arcone.service.domain.TaskCategory;

public interface TaskCategoryRepository extends CrudRepository<TaskCategory, Long>{
	
	TaskCategory findByCategoryNameIgnoreCase(String categoryName);
	Iterable<TaskCategory> findByActive(Boolean value);
	
}
