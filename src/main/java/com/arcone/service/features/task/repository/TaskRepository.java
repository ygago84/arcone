package com.arcone.service.features.task.repository;

import org.springframework.data.repository.CrudRepository;

import com.arcone.service.domain.Task;

public interface TaskRepository extends CrudRepository<Task, Long> {

}
