package com.arcone.service.features.task.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arcone.service.domain.Task;
import com.arcone.service.features.task.assembler.TaskAssembler;
import com.arcone.service.features.task.dto.TaskDto;
import com.arcone.service.features.task.repository.TaskRepository;
import com.arcone.service.features.task.service.TaskService;

@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
	TaskRepository taskRepository;

	@Override
	public Task save(TaskDto taskDto) throws Exception {
		Task task = TaskAssembler.toEntity(taskDto);
		return taskRepository.save(task);
	}

	@Override
	public void delete(Long taskId) throws Exception {
		taskRepository.deleteById(taskId);
	}
	
	@Override
    public Optional<Iterable<Task>> getAllTasks() throws Exception {
        return Optional.ofNullable(taskRepository.findAll());
    }

}
