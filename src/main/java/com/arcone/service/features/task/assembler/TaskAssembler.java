package com.arcone.service.features.task.assembler;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.arcone.service.domain.Task;
import com.arcone.service.features.task.dto.TaskDto;
import com.arcone.service.features.taskcategory.assembler.TaskCategoryAssembler;

public class TaskAssembler {
	public static TaskDto toDto(Optional<Task> taskOptional) {
		TaskDto dto = new TaskDto();
		taskOptional.ifPresent(task -> {
			dto.setId(task.getId());
			dto.setDate(task.getDate());
			dto.setTaskDescription(task.getTaskDescription());
			dto.setTimeSpent(task.getTimeSpent());
			dto.setCategory(TaskCategoryAssembler.toDto(Optional.of(task.getCategory())));
		});
		return dto;
	}

	public static TaskDto toDto(Task task) {
		TaskDto dto = new TaskDto();
		dto.setId(task.getId());
		dto.setDate(task.getDate());
		dto.setTaskDescription(task.getTaskDescription());
		dto.setTimeSpent(task.getTimeSpent());
		dto.setCategory(TaskCategoryAssembler.toDto(Optional.of(task.getCategory())));
		return dto;
	}

	public static List<TaskDto> toDtos(Optional<Iterable<Task>> tasksOptional) {
		List<TaskDto> taskDtos = new ArrayList<>();
		tasksOptional.ifPresent(taskCollection -> {
			taskCollection.forEach(task -> taskDtos.add(TaskAssembler.toDto(Optional.of(task))));
		});
		return taskDtos;
	}

	public static Task toEntity(TaskDto taskDto) {
		return null != taskDto.getId() ?
			new Task(taskDto.getId(), taskDto.getDate(), taskDto.getTimeSpent(), taskDto.getTaskDescription(), TaskCategoryAssembler.toEntity(taskDto.getCategory())) :
			new Task(taskDto.getDate(), taskDto.getTimeSpent(), taskDto.getTaskDescription(), TaskCategoryAssembler.toEntity(taskDto.getCategory()));
	}
}
