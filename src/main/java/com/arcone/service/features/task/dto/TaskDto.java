package com.arcone.service.features.task.dto;

import java.io.Serializable;
import java.util.Date;

import com.arcone.service.common.util.JsonDateDeserializer;
import com.arcone.service.common.util.JsonDateSerializer;
import com.arcone.service.features.taskcategory.dto.TaskCategoryDto;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class TaskDto implements Serializable {
    private static final long serialVersionUID = 8843810955626580920L;
    private Long id;
    private Date date;
    private String timeSpent;
    private String taskDescription;
    private TaskCategoryDto category;

    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @JsonSerialize(using = JsonDateSerializer.class)
    public Date getDate() {
        return date;
    }

    @JsonDeserialize(using = JsonDateDeserializer.class)
    public void setDate(Date date) {
        this.date = date;
    }

    public String getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(String timeSpent) {
        this.timeSpent = timeSpent;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }
    
	public TaskCategoryDto getCategory() {
		return category;
	}
	
	public void setCategory(TaskCategoryDto category) {
		this.category = category;
	}
	
	@Override
	public String toString() {
		return "TaskDto [id=" + id + ", date=" + date + ", timeSpent=" + timeSpent + ", taskDescription="
				+ taskDescription + ", category=" + category + "]";
	}
      
}
