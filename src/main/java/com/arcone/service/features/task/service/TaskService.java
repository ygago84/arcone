package com.arcone.service.features.task.service;

import java.util.Optional;

import com.arcone.service.domain.Task;
import com.arcone.service.features.task.dto.TaskDto;

public interface TaskService {
	public Task save(TaskDto taskDto) throws Exception;
	public void delete(Long taskId) throws Exception;
	public Optional<Iterable<Task>> getAllTasks() throws Exception;
}
