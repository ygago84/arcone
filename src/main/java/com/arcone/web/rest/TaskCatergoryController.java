package com.arcone.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.arcone.service.common.exception.ApplicationException;
import com.arcone.service.features.taskcategory.dto.TaskCategoryDto;
import com.arcone.service.published.TaskPublishedService;

@Controller
@RequestMapping("/api")
public class TaskCatergoryController {

	@Autowired
	private TaskPublishedService taskPublishedService;

	@PostMapping("/save-task-category")
	public @ResponseBody TaskCategoryDto saveTaskCategory(@RequestBody TaskCategoryDto taskCatgeoryDto)
			throws ApplicationException {
		return this.taskPublishedService.save(taskCatgeoryDto);
	}

	@GetMapping("/get-all-task-category")
	public @ResponseBody List<TaskCategoryDto> getAllTaskCategories() throws ApplicationException {
		return this.taskPublishedService.getAllCategories();
	}

	@DeleteMapping("/delete-category/{id}")
	public @ResponseBody Long deleteTaskCategory(@PathVariable Long id) throws ApplicationException {
		this.taskPublishedService.deleteTaskCategory(id);
		return id;
	}
}
