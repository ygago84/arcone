package com.arcone.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.arcone.service.common.exception.ApplicationException;
import com.arcone.service.features.task.dto.TaskDto;
import com.arcone.service.published.TaskPublishedService;

@Controller
@RequestMapping("/api")
public class TaskController {

	@Autowired
	private TaskPublishedService taskPublishedService;

	@PostMapping("/save-task")
	public @ResponseBody TaskDto saveTask(@RequestBody TaskDto taskDto) throws ApplicationException {
		return this.taskPublishedService.save(taskDto);
	}

	@GetMapping("/get-all-tasks")
	public @ResponseBody List<TaskDto> getAllTasks() throws ApplicationException {
		return this.taskPublishedService.getAllTasks();
	}

	@DeleteMapping("/delete-task/{id}")
	public @ResponseBody Long deleteTask(@PathVariable Long id) throws ApplicationException {
		this.taskPublishedService.deleteTask(id);
		return id;
	}

}
